const request = require('./request')
const DB = require('./db');
const dbConfig = {
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'user',
    password: process.env.DB_PASSWORD || 'password',
    database: process.env.DB_DATABASE || 'affluent'
}
const db = new DB();

(async () => {
    const sleep = time => new Promise(resolve => setTimeout(resolve, time));

    const connectDB = async () => {
        try {
            await db.connect(dbConfig);
        } catch (e) {
            await sleep(4000);
            await connectDB();
        }
    };

    await connectDB();

    try {
        await db.query(`
        CREATE TABLE IF NOT EXISTS users (
            id INT,
            email VARCHAR(256),
            first_name VARCHAR(256),
            last_name VARCHAR(256),
            avatar VARCHAR(512)
        )
    `);
    } catch (e) {
        console.error('Couldn\'t create table users', e);
        return;
    }

    let page = 1;
    while(true) {

        let result;
        try {
            result = await request(`https://reqres.in/api/users?page=${page}`);
            result = JSON.parse(result);

        } catch (e) {
            console.error(`error on fetching users on page ${page}`, e)
            break;
        }

        if(!result.data.length) {
            break;
        }

        for(let user of result.data) {
            try {
                await db.query('INSERT INTO users (id, email, first_name, last_name, avatar) VALUES(?, ? ,?, ?, ?)', [
                    user.id, user.email, user.first_name, user.last_name, user.avatar
                ]);
            } catch (e) {
                console.error(`error on writing users to database on page ${page}`, e);
                break;
            }
        }
        page++;
    }
    console.info('Finished fetching users');
})();