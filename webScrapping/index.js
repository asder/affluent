const puppeteer = require('puppeteer');
const DB = require('./db');
const dbConfig = {
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'user',
    password: process.env.DB_PASSWORD || 'password',
    database: process.env.DB_DATABASE || 'affluent'
}
const db = new DB();

(async () => {
    const sleep = time => new Promise(resolve => setTimeout(resolve, time));

    const connectDB = async () => {
        try {
            await db.connect(dbConfig);
        } catch (e) {
            await sleep(4000);
            await connectDB();
        }
    };

    await connectDB();

    try {
        await db.query(`
        CREATE TABLE IF NOT EXISTS affluent (
            date DATE,
            commissions_total FLOAT,
            sales_net INT,
            leads_net INT,
            clicks INT,
            epc FLOAT,
            impressions FLOAT,
            cr FLOAT
        )
    `);
    } catch (e) {
        console.error('Couldn\'t create table affluent', e);
        return;
    }

    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    const page = await browser.newPage();
    await page.goto('https://develop.pub.afflu.net/login');

    await page.evaluate((login, password) => {
        document.getElementsByName('username')[0].value = login;
        document.getElementsByName('password')[0].value = password;
        document.querySelector('.login-form [type="submit"]').click()
    }, 'developertest@affluent.io', 'Wn4F6g*N88EPiOyW');
    await page.waitForNavigation();

    await page.goto('https://develop.pub.afflu.net/list?type=dates&startDate=2020-10-01&endDate=2020-10-30');

    await page.waitForSelector('.dataTables_paginate li.active');

    let pageNum = 1;

    while(true) {
        const result = await page.evaluate(() => {
            const rowMap = {
                0: {
                    name: 'date',
                    getData: (td) => {
                        const href = td.getElementsByTagName('a')[0].getAttribute('href');
                        return href.match(/startDate=([^&]+)/)[1];
                    }
                },
                1: {
                    name: 'commissions_total',
                    getData: (td) => {
                        return td.innerHTML.match(/\$([^<]+)/)[1].trim().replace(',', '');
                    }
                },
                2: {
                    name: 'sales_net',
                    getData: (td) => {
                        return td.innerHTML.trim().replace(',', '');
                    }
                },
                3: {
                    name: 'leads_net',
                    getData: (td) => {
                        return td.innerHTML.trim().replace(',', '');
                    }
                },
                4: {
                    name: 'clicks',
                    getData: (td) => {
                        return td.innerHTML.trim().replace(',', '');
                    }
                },
                5: {
                    name: 'epc',
                    getData: (td) => {
                        return td.innerHTML.match(/\$([^<]+)/)[1].trim().replace(',', '');
                    }
                },
                6: {
                    name: 'impressions',
                    getData: (td) => {
                        return td.innerHTML.trim().replace(',', '');
                    }
                },
                7: {
                    name: 'cr',
                    getData: (td) => {
                        return td.innerHTML.match(/([^%]+)/)[1].trim();
                    }
                },
            };
            const dataRows = Array.from(document.querySelectorAll('.dataTable tbody tr')).map((tr) => {
                return Array.from(tr.querySelectorAll('td')).reduce((acc, td, i) => {
                    acc[rowMap[i].name] = rowMap[i].getData(td);
                    return acc;
                }, {});
            });
            const isNextDisabled = Boolean(document.querySelector('.dataTables_paginate .next.disabled'));

            return {
                isNextDisabled,
                dataRows
            };
        });

        for(let row of result.dataRows) {
            try {
                await db.query('INSERT INTO affluent (date, commissions_total, sales_net, leads_net, clicks, epc, impressions, cr) VALUES(?, ? ,?, ?, ?, ?, ?, ?)', [
                    row.date, row.commissions_total, row.sales_net, row.leads_net, row.clicks, row.epc, row.impressions, row.cr
                ]);
            } catch (e) {
                console.error(`error on writing data to database`, e);
                break;
            }
        }

        if(result.isNextDisabled) {
            break;
        }

        await page.evaluate(() => {
            document.querySelector('.dataTables_paginate [title="Next"]').click();
        });

        pageNum++;

        await page.waitForFunction((pageNum) => {
            return document.querySelector('.dataTables_paginate li.active').innerText.trim() === String(pageNum);
        }, {}, pageNum);
    }
    await browser.close();
    console.info('Finished scrapping data');
})();