const mysql = require( 'mysql2/promise' );

class DB {
    #connection
    constructor() {
    }

    async connect(config) {
        this.#connection = await mysql.createConnection(config);
    }

    query(sql, args) {
        return this.#connection.execute(sql, args);
    }

    close() {
        this.#connection.end();
    }
}

module.exports = DB;