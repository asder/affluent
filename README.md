## Affluent

### Usage
#### to run apps be sure docker is installed then do command
```
docker-compose up
```
#### when scrapping is ready you will see texts
```
Finished fetching users
Finished scrapping data
```

#### to see the results go to mysql container
```
docker exec -it <mysql_mycontainer> bash
mysql -uuser -ppassword
```
#### run sql commands
```
use affluent
select * from users;
select * from affluent;
```